package com.example.datetimepickerexample

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_snack_bar.*

class SnackBarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_snack_bar)
    }

    fun openSnackBar(view:View){
        val snackbar = Snackbar.make(view,"Snackbar is open for service",Snackbar.LENGTH_LONG)
            .setAction("Still hungry??"){
                view ->
                Toast.makeText(this,"hello",Toast.LENGTH_LONG).show();
            }
        //getting resources from R.color
        var i = this.resources.getColor(R.color.colorAccent,theme)
        snackbar.setActionTextColor(i)
        snackbar.show()
    }

}
