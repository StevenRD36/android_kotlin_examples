package com.example.datetimepickerexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_progress.*

class ProgressActivity : AppCompatActivity() {


    var inProgress = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress)
        btn_download3.setOnClickListener {
            Thread(
                Runnable {
                    var progressBarStatus = 0
                    var rate = 0
                    while (progressBarStatus < 100) {
                        try {
                            rate += 10
                            Thread.sleep(500)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                        progressBarStatus = rate
                        progressBar3.progress = progressBarStatus
                    }
                    progressBar3.visibility = ProgressBar.INVISIBLE
                }
            ).start()
        }
    }

    fun toggle(view:View){
        if(progressBar2.visibility == ProgressBar.VISIBLE && progressBar.visibility == ProgressBar.VISIBLE){
            progressBar2.visibility = ProgressBar.INVISIBLE
            progressBar.visibility = ProgressBar.INVISIBLE
        }
        else{
            progressBar.visibility = ProgressBar.VISIBLE
            progressBar2.visibility = ProgressBar.VISIBLE
        }
    }

}

